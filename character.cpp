#include <cstdio>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include "datatypes.h"
#include "character.h"
#include "maploader.h"

character::character(int xPos, int yPos, int terminalVelocity) {
	this->xPos = xPos;
	this->yPos = yPos;
	this->xVel = this->yVel = 0;
	this->width = this->height = 30;
	this->terminalVelocity = terminalVelocity;
	this->moving[MOVE_ACTION_LEFT] = this->moving[MOVE_ACTION_RIGHT] = false;
	this->moving[MOVE_ACTION_FALL] = true;
}

void character::reset(void) {
	this->moving[MOVE_ACTION_FALL] = true;
	this->xPos = 50;
	this->yPos = 0;
}

void character::setMovement(int movementAction) {
	if(movementAction == MOVE_ACTION_JUMP && !this->moving[movementAction]) {
		this->yPos -= 1;
		this->yVel = -15;
	}
	
	this->moving[movementAction] = true;
	this->xVel = 0;
}

void character::unsetMovement(int movementAction) {
	this->moving[movementAction] = false;
	this->xVel = 0;
}

void character::crouch() {
	int halfSize = this->height / 2;
	this->yPos += halfSize;
	this->height -= halfSize;
}

void character::stand() {
	this->yPos -= this->height;
	this->height *= 2;
}

void character::move(mapLoader &gameMap){
	if(this->moving[MOVE_ACTION_JUMP] || this->moving[MOVE_ACTION_FALL]) {
		if(this->moving[MOVE_ACTION_FALL]) {
			/*for(int i = 0; i < platformCount; i++) {
				platform curPlatform = gamePlatforms[i];
				curPlatform.xPos -= (curPlatform.thickness / 2);
				curPlatform.height += (curPlatform.thickness * 2);
				curPlatform.yPos -= (curPlatform.thickness / 2);
				curPlatform.width += (curPlatform.thickness * 2);
				int charFloor = this->yPos + this->height;
				int charRightBound = this->xPos + this->width;
			
				if(((charRightBound >= curPlatform.xPos) && (this->xPos <= (curPlatform.xPos + curPlatform.width))) &&
				   ((charFloor >= curPlatform.yPos) && (charFloor <= (curPlatform.yPos + curPlatform.height)))) {
					this->yPos = curPlatform.yPos - this->height;
					this->yVel = 0;
					this->standingPlatform = i;
					this->moving[MOVE_ACTION_JUMP] = false;
					this->moving[MOVE_ACTION_FALL] = false;
				}
			}*/
		}
		
		if(this->moving[MOVE_ACTION_JUMP] || this->moving[MOVE_ACTION_FALL]) {
			if(this->yVel < this->terminalVelocity) {
				this->yVel += (gravityForce / gameFPS);
			} else
				if(!this->moving[MOVE_ACTION_FALL])
					this->moving[MOVE_ACTION_FALL] = true;
		}
	} else {
		/*if(((this->xPos + this->width) <= gamePlatforms[this->standingPlatform].xPos) ||
		   (this->xPos >= (gamePlatforms[this->standingPlatform].xPos + gamePlatforms[this->standingPlatform].width))) {
			this->moving[MOVE_ACTION_FALL] = true;
		}*/
	}
	
	if(this->moving[MOVE_ACTION_LEFT])
		this->xVel = -3;
	
	if(this->moving[MOVE_ACTION_RIGHT])
		this->xVel = 3;
}

void character::update(void) {
	if(this->moving[MOVE_ACTION_JUMP] || this->moving[MOVE_ACTION_FALL])
		this->yPos += this->yVel;
	
	this->xPos += this->xVel;
}

void character::draw(void) {
	al_draw_filled_rectangle(this->xPos,
						    this->yPos,
						    (this->xPos + this->width),
						    (this->yPos + this->height),
						    al_map_rgb(255, 255, 255));
}
