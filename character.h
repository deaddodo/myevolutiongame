#include "maploader.h"

#ifndef _CHARACTER_H_
#define _CHARACTER_H_
class character {
	private:
		int width,
			height,
			standingPlatform,
		    terminalVelocity;
		float xPos,
			  yPos,
			  xVel,
			  yVel;
		bool isFalling,
			 moving[MOVE_ACTION_COUNT];
	public:
		character(int, int, int);
		void reset(void);
		void setMovement(int);
		void unsetMovement(int);
		void crouch(void);
		void stand(void);
		void move(mapLoader &);
		void update(void);
		void draw(void);
};
#endif