#include <cstdio>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include "datatypes.h"
#include "character.h"
#include "maploader.h"

int main(int argc, char **argv) {
	ALLEGRO_DISPLAY *gameDisplay = NULL;
	ALLEGRO_TIMER *gameTimer = NULL;
	ALLEGRO_EVENT_QUEUE *gameEventQueue = NULL;
	bool gameRunning = true;
	character userPlayer(530, 0, charTermVelocity);
	mapLoader gameMap;
	
	if(!al_init())
		return -1;
	
	gameDisplay = al_create_display(screenWidth, screenHeight);
	if(!gameDisplay)
		return -1;
	
	al_install_keyboard();
	al_init_primitives_addon();
	
	gameEventQueue = al_create_event_queue();
	gameTimer = al_create_timer(1.0 / gameFPS);
	al_register_event_source(gameEventQueue, al_get_timer_event_source(gameTimer));
	al_register_event_source(gameEventQueue, al_get_keyboard_event_source());
	
	gameMap.load("./resources/maps/01.map");
	
	al_start_timer(gameTimer);
	
	while(gameRunning) {
		ALLEGRO_EVENT gameEvent;
		al_wait_for_event(gameEventQueue, &gameEvent);
		
		if(gameEvent.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch(gameEvent.keyboard.keycode) {
				case ALLEGRO_KEY_ESCAPE:
					gameRunning = false;
					break;
				case ALLEGRO_KEY_R:
					userPlayer.reset();
					break;
				case ALLEGRO_KEY_LEFT:
					userPlayer.setMovement(MOVE_ACTION_LEFT);
					break;
				case ALLEGRO_KEY_RIGHT:
					userPlayer.setMovement(MOVE_ACTION_RIGHT);
					break;
				case ALLEGRO_KEY_DOWN:
					userPlayer.crouch();
					break;
				case ALLEGRO_KEY_SPACE:
					userPlayer.setMovement(MOVE_ACTION_JUMP);
					break;
			}
		}
		
		if(gameEvent.type == ALLEGRO_EVENT_KEY_UP) {
			switch(gameEvent.keyboard.keycode) {
				case ALLEGRO_KEY_LEFT:
					userPlayer.unsetMovement(MOVE_ACTION_LEFT);
					break;
				case ALLEGRO_KEY_RIGHT:
					userPlayer.unsetMovement(MOVE_ACTION_RIGHT);
					break;
				case ALLEGRO_KEY_DOWN:
					userPlayer.stand();
					break;
			}
		}
		
		if(gameEvent.type == ALLEGRO_EVENT_TIMER) {
			for(int i = 0; i < gameMap.getHeight(); i++) {
				for(int j = 0; j < gameMap.getWidth(); j++) {
					if(gameMap.getTile(j, i) == 1 || gameMap.getTile(j, i)) {
						int xPos = j * tileSize;
						int yPos = i * tileSize;
						int height = (gameMap.getTile(j, i) == 2) ? (tileSize / 2) : tileSize;
						al_draw_filled_rectangle(xPos, yPos, xPos + tileSize, yPos + height, al_map_rgb(0xa9, 0xa9, 0xa9));
					}
				}
			}
			
			userPlayer.move(gameMap);
			userPlayer.update();
			userPlayer.draw();
			
			al_flip_display();
			al_clear_to_color(al_map_rgb(0x58, 0x58, 0x58));
		}
	}
	
	al_stop_timer(gameTimer);
	al_destroy_timer(gameTimer);
	gameMap.destroy();
	al_destroy_event_queue(gameEventQueue);
	al_destroy_display(gameDisplay);
	
	return 0;
}
