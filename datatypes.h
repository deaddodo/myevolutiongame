#ifndef _DATATYPES_H_
#define _DATATYPES_H_
static const int screenWidth = 800;
static const int screenHeight = 640;
static const int gameFPS = 60;
static const int gravityForce = 60;
static const int charTermVelocity = 8;
static const int gamePlatformCount = 14;
static const int tileSize = 16;

static const int MOVE_ACTION_COUNT = 4;
static const int MOVE_ACTION_JUMP = 0;
static const int MOVE_ACTION_LEFT = 1;
static const int MOVE_ACTION_RIGHT = 2;
static const int MOVE_ACTION_FALL = 3;

struct platform {
	int xPos,
		yPos,
		width,
		height,
		thickness;
};
#endif