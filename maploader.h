#pragma once
class mapLoader {
	private:
		int *mapData;
		int width, height, mapSize;
		void initMapData(int);
	public:
		mapLoader(void);
		bool load(const char*);
		int getTile(int, int);
		void destroy(void);
		int getWidth(void);
		int getHeight(void);
};