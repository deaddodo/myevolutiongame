#include <cstdio>
#include <cstdlib>
#include <fstream>
#include "maploader.h"

mapLoader::mapLoader(void) {
	this->mapData = NULL;
	this->width = this->height = this->mapSize = 0;
}

void mapLoader::initMapData(int size) {
	this->mapData = new int[size];
	for(int i = 0; i < size; i++) {
		
	}
}

bool mapLoader::load(const char *fileName){
	std::ifstream mapFile(fileName);
	
	if(mapFile.is_open()) {
		int itemCount = 0;
		
		mapFile >> this->width >> this->height;
		this->mapSize = (this->width * this->height);
		
		this->initMapData(this->mapSize);
		
		while(!mapFile.eof() && (itemCount < this->mapSize)) {
			mapFile >> this->mapData[itemCount];
			itemCount++;
		}
		
		mapFile.close();
		return true;
	} else
		return false;
}

int mapLoader::getWidth(void) {
	return this->width;
}

int mapLoader::getHeight(void) {
	return this->height;
}

void mapLoader::destroy(void) {
	delete [] this->mapData;
	this->mapData = NULL;
}

int mapLoader::getTile(int x, int y) {
	int position = ((y * this->width) + x);
	if(position < this->mapSize)
		return this->mapData[position];
	else
		return 0;
}