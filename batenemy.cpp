#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

#include "datatypes.h"
#include "batenemy.h"

struct xyPos {
	int x,
		y;
};

struct wingFrame {
	xyPos point1,
		  point2,
		  point3;
};

batEnemy::batEnemy(void) {
	this->frameNumber[BAT_WINGS] = 1;
	this->animationReversed[BAT_WINGS] = false;
}

void batEnemy::update(void) {
	
}

void batEnemy::draw(void) {
	al_draw_filled_circle(200, 300, 10, al_map_rgb(0x1e, 0x1e, 0x58));
	al_draw_filled_triangle(190, 275, 210, 275, 200, 295, al_map_rgb(0x45, 0x45, 0xca));
}